﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BeerShootingBot 
{
    public static class RconManager 
    {
        /// <summary>
        /// Queue of all commands
        /// </summary>
        static Queue<string> rconQueue = new Queue<string>();

        public static async void StartQueueLoop(Socket socket, string password) 
        {
            while(true) 
            {
                await Task.Delay(500);
                if (rconQueue.Count > 0)
                    Send(socket, $"rcon {password} {rconQueue.Dequeue()}");
            }
        }

        public static void BigText(string msg) 
        {
            QueueCommand($"bigtext \"{msg}\"");
        }
        public static void QueueCommand(string command)
        {
            Log.WriteLine($"rcon: {command}");
            rconQueue.Enqueue(command);
        }

        /// <summary>
        /// Connects to remote game server
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="port"></param>
        /// <returns>Returns the socket</returns>
        public static Socket Connect(string ip, int port) 
        {
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            try 
            {
                //attempts to connect
                socket.Connect(IPAddress.Parse(ip), port);
                Log.WriteLine($"Connected to ({ip}:{port})");
            }
            catch (Exception e)
            {
                //connect failed
                Log.WriteLine($"<Error> Can't connect to ({ip}:{port})");
                return null;
            }

            return socket;
        }

        /// <summary>
        /// Calls <b>GenerateByteArray</b> and then sends the command
        /// </summary>
        /// <param name="command"></param>
        /// <returns>Response from the server</returns>
        static string Send(Socket socket, string command) 
        {
            //generates payload
            byte[] payload = GenerateByteArray(command);

            //send rcon command and get response
            IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
            socket.Send(payload, SocketFlags.None);

            //big enough to receive response
            byte[] bufferRec = new byte[65000]; //originally 65000, not 65
            
            socket.Receive(bufferRec);

            //removes 0 bytes at the end
            byte[] filtered = bufferRec.Reverse().SkipWhile(x => x > 32 && x < 126).Reverse().ToArray();
            string response = Encoding.ASCII.GetString(filtered);

            return response;
        }

        static byte[] GenerateByteArray(string toAppend) 
        {
            byte[] bufferTemp = Encoding.ASCII.GetBytes(toAppend);
            byte[] bufferSend = new byte[bufferTemp.Length + 5];

            //intial 5 characters as per standard
            bufferSend[0] = byte.Parse("255");
            bufferSend[1] = byte.Parse("255");
            bufferSend[2] = byte.Parse("255");
            bufferSend[3] = byte.Parse("255");
            bufferSend[4] = byte.Parse("255");
            int j = 4;

            for (int i = 0; i < bufferTemp.Length; i++) {
                bufferSend[j++] = bufferTemp[i];
            }

            return bufferSend;
        }

        public static bool TestAndFetchConnection(Socket socket, string password) 
        {
            try 
            {
                Log.WriteLine("Testing the connection");
                string response = Send(socket, $"rcon {password} status");
                //PrintResponse(response);
                if (response.Contains("Bad rconpassword."))
                    Log.WriteLine("Bad rcon password, please set the right password");
                else
                {
                    Parser.ParseStatus(response);
                    Log.WriteLine("Successful connection");
                }
                return true;
            }
            catch (Exception e)
            {
                Log.WriteLine($"<Error> can't connect to the server, the server may not exist ; full message: {e.Message}"); 
                return false;
            }
        }

        public static void PrintResponse(string response) 
        {
            int spaceAmount = 0;

            foreach(var c in response) 
            {
                if (spaceAmount <= 5 && c != (char)0) {
                    Console.Write(c);
                    spaceAmount = 0;
                }
                else if(spaceAmount <= 5 && c == (char)0) 
                {
                    Console.Write(c);
                    spaceAmount++;
                }
            }
            Console.Write("\n");
        }
    }
}

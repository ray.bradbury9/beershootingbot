﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerShootingBot 
{
    class Program 
    {
        static void Main(string[] args) 
        {
            Bot.Initialize();
            Bot.RunAsync();

            while(!Bot.ShouldClose) 
            {
                if(Console.ReadKey(true).Key == ConsoleKey.E) 
                {
                    break;
                }
            }

            Bot.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BeerShootingBot 
{
    /// <summary>
    /// Parses a line and right call callbacks
    /// </summary>
    public class Parser
    {
        public static Dictionary<string, IEnumerable<Func<int,string, bool>>> Commands = new Dictionary<string, IEnumerable<Func<int, string, bool>>>();
        static Dictionary<KILLED_BY, string> killedByTranslation = new Dictionary<KILLED_BY, string>();
        public static void Initialize() 
        {
            killedByTranslation.Add(KILLED_BY.UT_MOD_KNIFE, "knife");
            killedByTranslation.Add(KILLED_BY.UT_MOD_KNIFE_THROWN, "knife thrown");
            killedByTranslation.Add(KILLED_BY.UT_MOD_DEAGLE, "de");
            killedByTranslation.Add(KILLED_BY.UT_MOD_BERETTA, "beretta");
            killedByTranslation.Add(KILLED_BY.UT_MOD_COLT1911, "colt");
            killedByTranslation.Add(KILLED_BY.UT_MOD_UMP45, "ump");
            killedByTranslation.Add(KILLED_BY.UT_MOD_MP5K, "mp5");
            killedByTranslation.Add(KILLED_BY.UT_MOD_SPAS, "spas");
            killedByTranslation.Add(KILLED_BY.UT_MOD_M4, "m4");
            killedByTranslation.Add(KILLED_BY.UT_MOD_AK103, "ak");
            killedByTranslation.Add(KILLED_BY.UT_MOD_G36, "g36");
            killedByTranslation.Add(KILLED_BY.UT_MOD_LR300, "lr");
            killedByTranslation.Add(KILLED_BY.UT_MOD_NEGEV, "negev");
            killedByTranslation.Add(KILLED_BY.UT_MOD_PSG1, "psg");
            killedByTranslation.Add(KILLED_BY.UT_MOD_SR8, "sr8");
            killedByTranslation.Add(KILLED_BY.UT_MOD_HK69, "hk69");
            killedByTranslation.Add(KILLED_BY.UT_MOD_HK69_HIT, "<3");
            killedByTranslation.Add(KILLED_BY.UT_MOD_HEGRENADE, "he");
            killedByTranslation.Add(KILLED_BY.UT_MOD_BLED, "bled");
            killedByTranslation.Add(KILLED_BY.MOD_CHANGE_TEAM, "team change");
            killedByTranslation.Add(KILLED_BY.MOD_SUICIDE, "suicide <3");

        }

        public static void ParseClientUserInfoChanged(string line)
        {
            // 1 n\Steve\t\1\r\2\tl\0\f0\\f1\\f2\\a0\128\a1\128\a2\128   
            int id = Convert.ToInt32(line.Split(' ')[1]);
            if(GameManager.currentPlayers.ContainsKey(id))
            {
                var pl = GameManager.currentPlayers[id];
                var team = line.Split('\\')[3];
                pl.Team = (GameTeam)Convert.ToInt32(team);
            }
            else
            {
                Console.WriteLine($"<error>: can't find id on ClientUserInfoChanged({id})");
            }
        }

        //1:02 ClientUserinfo: 1 \ip\192.168.0.17:27961\challenge\1389749019\qport\2188\protocol\68\snaps\20\name\Heyo\racered\2\raceblue\2\racefree\0\rate\25000\ut_timenudge\0\cg_rgb\128 128 128\cg_physics\1\cg_ghost\1\cg_autopickup\-1\sex\male\handicap\100\color2\5\color1\4\authc\0\cl_guid\303F20DC7CA6121847FA498EA49F61C6\gear\GLAORWA\weapmodes\0000011022000002000200000000
        public static void ParseClientUserInfo(string line) 
        {
            Console.WriteLine("Parsing: ClientUserinfo");
            Dictionary<string, string> dataPairs = new Dictionary<string, string>();
            int from = 0;
            int separatorCount = 0;
            for (int i = 0; i < line.Length; i++) 
            {
                if(line[i] == '\\')
                    separatorCount++;

                if (line[i] == '\\' && (separatorCount == 3 || (separatorCount > 3 && separatorCount % 2 == 1))) 
                {
                    string rawLine = line.Substring(from, i - from);
                    if (rawLine[0] == '\\')
                    {
                        rawLine = rawLine.Substring(1);
                    }
                    else
                    {
                        rawLine = rawLine.Substring(rawLine.Split('\\').First().Length + 1);
                    }

                    var pair = rawLine.Split('\\');
                    dataPairs.Add(pair[0], pair[1]);
                    
                    from = i;
                }
            }

            string playerIDRaw = line.Split('\\').First();
            int playerID = -1;
            if(int.TryParse(playerIDRaw, out playerID))
            {
                if (playerID != -1)
                {
                    Player currentPlayer = null;

                    if (GameManager.currentPlayers.ContainsKey(playerID))
                    {
                        currentPlayer = GameManager.currentPlayers[playerID];
                    }
                    else if (playerID != -1 && !GameManager.currentPlayers.ContainsKey(playerID))
                    {
                        currentPlayer = new Player();
                        GameManager.currentPlayers.Add(playerID, currentPlayer);
                    }

                    if (currentPlayer != null)
                    {
                        currentPlayer.GameSessionId = playerID;

                        dataPairs.TryGetValue("ip", out currentPlayer.IP);
                        dataPairs.TryGetValue("chWallenge", out currentPlayer.ChWallenge);
                        dataPairs.TryGetValue("qport", out currentPlayer.Qport);
                        dataPairs.TryGetValue("protocol", out currentPlayer.Protocol);
                        dataPairs.TryGetValue("snaps", out currentPlayer.Snaps);
                        dataPairs.TryGetValue("name", out currentPlayer.Name);
                        dataPairs.TryGetValue("racered", out currentPlayer.Racered);
                        dataPairs.TryGetValue("raceblue", out currentPlayer.Raceblue);
                        dataPairs.TryGetValue("racefree", out currentPlayer.Racefree);
                        dataPairs.TryGetValue("rate", out currentPlayer.Rate);
                        dataPairs.TryGetValue("ut_timenudge", out currentPlayer.Ut_timenudge);
                        dataPairs.TryGetValue("cg_rgb", out currentPlayer.Cg_rgb);
                        dataPairs.TryGetValue("cg_physics", out currentPlayer.Cg_physics);
                        dataPairs.TryGetValue("cg_ghost", out currentPlayer.Cg_ghost);
                        dataPairs.TryGetValue("cg_autopickup", out currentPlayer.Cg_autopickup);
                        dataPairs.TryGetValue("sex", out currentPlayer.Sex);
                        dataPairs.TryGetValue("handicap", out currentPlayer.Handicap);
                        dataPairs.TryGetValue("color2", out currentPlayer.Color2);
                        dataPairs.TryGetValue("color1", out currentPlayer.Color1);
                        dataPairs.TryGetValue("authc", out currentPlayer.Authc);
                        dataPairs.TryGetValue("cl_guid", out currentPlayer.Cl_guid);
                        dataPairs.TryGetValue("gear", out currentPlayer.Gear);
                        dataPairs.TryGetValue("weapmodes", out currentPlayer.Weapmodes);
                        
                        
                    }
                }
            }
        }

        static string StripClientInfoPair(string pair) 
        {
            return pair.Split('\\')[2];
        }

        public static void ParseCoomandLine(int id, string line) 
        {
            var sections = line.Split(' ');
            if (sections.Length > 0 && Commands.ContainsKey(sections[0].ToLower())) 
            {
                var offset = sections[0].Length + 1;
                var cmds = Commands[sections[0].ToLower()];
                foreach(var x in cmds) 
                {
                    x.Invoke(id, offset >= line.Length ? line : line.Substring(offset));
                }
            }
        }

        public static void ParseKillBy(string line) 
        {
            // 0 1 19: BST|Kipash killed TestName54 by UT_MOD_LR300
            string killedbyRaw = line.Split(' ').Last();
            string idSection = line.Split(':').First();
            string[] ids = idSection.Split(' ');



            KILLED_BY killedby;
            if(Enum.TryParse(killedbyRaw, out killedby) && killedByTranslation.ContainsKey(killedby))
            {
                int killerID = Convert.ToInt32(ids[1]);
                int victimID = Convert.ToInt32(ids[2]);
                GameManager.OnKill(killerID, victimID, killedby);
                RconManager.BigText(Regex.Replace(line, killedby.ToString(), killedByTranslation[killedby]).Split(':').Last());
            }
        }

        public static void ParseStatus(string status)
        {
            /*
            ???print
            map: ut4_dressingroom
            num score ping name            lastmsg address               qport rate
            --- ----- ---- --------------- ------- --------------------- ----- -----
              0     0    0 BST|Kipash^7            0 loopback              35766 99999
            */

            var lines = status.Split('\n');
            var mapLine = lines.First(x => x.Contains("map:"));
            GameManager.CurrentMap = mapLine.Split(' ').Last();
            Console.WriteLine($"Map: {GameManager.CurrentMap}");

            for (int i = 4; i < lines.Length - 2; i++)
            {
                //Console.WriteLine($">>> {lines[i]}");
                string line = lines[i];

                var sections = Regex.Replace(line, @"\s+", " ").Split(' ');
                int plID = Convert.ToInt32(sections[1]);
                string score = sections[2];
                string name = sections[4];
                string ip = sections[6];

                Console.WriteLine($"Adding: {name}({plID}) - {score} - {ip}");

                Player currentPlayer;
                if (!GameManager.currentPlayers.TryGetValue(plID, out currentPlayer))
                {
                    currentPlayer = new Player();
                }

                currentPlayer.GameSessionId = plID;
                currentPlayer.Name = name;
                currentPlayer.IP = ip;

                if(!GameManager.currentPlayers.ContainsKey(plID))
                {
                    GameManager.AddPlayer(currentPlayer);
                }
            }
        }
    }
}

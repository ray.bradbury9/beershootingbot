﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace BeerShootingBot 
{
    /// <summary>
    /// Main class where everything happens
    /// </summary>
    public static class Bot 
    {
        const string IP = "192.168.0.17";
        const int Port = 27960;
        const string Password = "999";
        const string LogPath = @"C:\Users\Kip\AppData\Roaming\Quake3\q3ut4\game.log";

        static Socket serverSocket;

        public static bool ShouldClose { get; private set; }

        public static void Initialize() 
        {
            ShouldClose = false;

            Console.Title = "--- BeerShooting Bot v0.0.1 ---";

            Log.Add(new ConsoleLogger());
            Log.WriteLine(Console.Title);
            Log.WriteLine("--- press E to close connection and properlly exit ---");

            serverSocket = RconManager.Connect(IP, Port);

            Commands.Initialize();
            GameLog.Initialize();
            Parser.Initialize();

            //Tests connection if it exists
            if (serverSocket == null || !RconManager.TestAndFetchConnection(serverSocket, Password))
                ShouldClose = true;
        }

        /// <summary>
        /// Initializes loops
        /// </summary>
        public static void RunAsync() 
        {
            if (ShouldClose)
                return;

            Log.WriteLine("Initializing loops");
            RconManager.StartQueueLoop(serverSocket, Password);
            GameLog.StartLogLoop(LogPath);
        }


        /// <summary>
        /// Clouse all loops and connections
        /// </summary>
        public static void Close() 
        {
            Log.WriteLine("Clousing connections");

            if(serverSocket != null)
                serverSocket.Close();
        }

    }
}

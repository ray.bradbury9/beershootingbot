﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerShootingBot 
{
    public enum LogEvent 
    {
        none = 0, //default

        ClientConnect, //user connected
        ClientDisconnect, //user disconnected
        ClientUserinfo, //info about a player
        ClientUserinfoChanged, //joined team etc.

        Kill, //somebody kileld somebody

        say, //public chat

        Exit, //match ends
        ShutdownGame,//match ends
    }
}

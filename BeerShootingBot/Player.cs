﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerShootingBot 
{
    public class Player 
    {
        public bool IsBot;

        public int GameSessionId = -1;
        public string IP = "";
        public string ChWallenge = "";
        public string Qport = "";
        public string Protocol = "";
        public string Snaps = "";
        public string Name = "";
        public string Racered = "";
        public string Raceblue = "";
        public string Racefree = "";
        public string Rate = "";
        public string Ut_timenudge = "";
        public string Cg_rgb = "";
        public string Cg_physics = "";
        public string Cg_ghost = "";
        public string Cg_autopickup = "";
        public string Sex = "";
        public string Handicap = "";
        public string Color2 = "";
        public string Color1 = "";
        public string Authc = "";
        public string Cl_guid = "";
        public string Gear = "";
        public string Weapmodes = "";

        public GameTeam Team;

        public int Kills = 0;
        public int Deaths = 0;

        public List<KILLED_BY> KillsVia = new List<KILLED_BY>();

        public override string ToString() 
        {
            return $"{Name}({GameSessionId}) - t: {Team}";    
        }
    }
}

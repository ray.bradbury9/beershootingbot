﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerShootingBot 
{

    public static class Log 
    {
        static List<ILog> loggers = new List<ILog>();
        public static void Add(ILog log) 
        {
            loggers.Add(log);
        }

        public static void WriteLine(string msg) 
        {
            for (int i = 0; i < loggers.Count; i++) 
            {
                loggers[i].WriteLine(msg);
            }
        }
    }

    /// <summary>
    /// Base interface for every logger
    /// </summary>
    public interface ILog 
    {
        void WriteLine(string msg);
    }

    /// <summary>
    /// Logger for a log.txt file
    /// </summary>
    public class FileLogger : ILog 
    {
        public void WriteLine(string msg) 
        {
            //TODO: implement
        }
    }

    /// <summary>
    /// Logger for the console
    /// </summary>
    public class ConsoleLogger : ILog 
    {
        public void WriteLine(string msg) 
        {
            Console.WriteLine(msg);
        }
    }
}

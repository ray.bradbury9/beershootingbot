﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerShootingBot 
{
    /// <summary>
    /// Loads <b>game.log</b> and feed line by line into <b>Parser</b>
    /// </summary>
    public class GameLog 
    {
        /// <summary>
        /// Frequency in which the main game.log file will be read. In Milliseconds.
        /// </summary>
        const int FileReadFrequency = 100;
        
        static Dictionary<LogEvent, Action<string>> logEvents = new Dictionary<LogEvent, Action<string>>();
        
        public static void Initialize() 
        {
            logEvents.Add(LogEvent.say, (line) => 
            {
                //event|id| name   |  msg
                //say: 0 BST|Kipash: !moon on

                //Console.WriteLine($"orig msg: {line}");
                
                var sections = line.Split(' ');
                var offset = sections[0].Length + sections[1].Length + sections[2].Length + 3;

                var name = sections[2].Remove(sections[2].Length - 1);
                var id = sections[1];
                var msg = line.Substring(offset);
                

                //Console.WriteLine($"{name}({id}): {msg}");

                //'!' actual in-game command prefix
                if(msg[0] == '!') 
                {
                    Parser.ParseCoomandLine(Convert.ToInt32(id), msg.Substring(1));
                }
            });

            logEvents.Add(LogEvent.Kill, (line) => {

                Parser.ParseKillBy(line);
            });

            logEvents.Add(LogEvent.ClientUserinfo, (line) => {
                Parser.ParseClientUserInfo(line);
            });

            logEvents.Add(LogEvent.ClientUserinfoChanged, (line) =>
            {
                Parser.ParseClientUserInfoChanged(line);
            });

            logEvents.Add(LogEvent.ClientDisconnect, (line) => {
                var sessionID = line.Replace(" ", "");
                Console.WriteLine($"({sessionID})");
                GameManager.RemovePlayer(Convert.ToInt32(sessionID));
            });
        }
        
        public static string ReadLog(string logPath) 
        {
            FileStream logStream = new FileStream(logPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            StreamReader logReader = new StreamReader(logStream);

            string log = logReader.ReadToEnd();
            return log;
        }

        static DateTime lastTimestamp = new DateTime();
        static int lastValidLineIndex;
        static int lastSessionSize;
        public static async void StartLogLoop(string logPath)
        {
            bool firstIteration = true; 
            while(true) 
            {
                await Task.Delay(FileReadFrequency);
                
                var log = ReadLog(logPath); //returns the raw file
                var linesUnfiltered = log.Split('\n').Where(x => !string.IsNullOrEmpty(x)); // creates lines from one long string


                //Idiotic
                List<string> lines = new List<string>();
                foreach(var x in linesUnfiltered) 
                {
                    if(x.Contains("------------------------------------------------------------"))
                    {
                        lines.Clear();
                    }
                    lines.Add(x);
                }
                
                if(lines.Count < lastSessionSize) 
                {
                    lastTimestamp = new DateTime();
                    lastValidLineIndex = 0;
                    lastSessionSize = lines.Count;

                    Console.WriteLine("Restart!");
                }
                lastSessionSize = lines.Count;

                for (int i = 0; i < lines.Count; i++) 
                {
                    var line = lines.ElementAt(i);

                    //time |event|id| name     |  msg
                    //  0:54 say: 0 BST|Kipash: !moon on

                    var time = line.Substring(0, 7);
                    var dt = Convert.ToDateTime(time);
                    if (dt >= lastTimestamp && i > lastValidLineIndex) 
                    {
                        lastTimestamp = dt;
                        lastValidLineIndex = i;

                        if (firstIteration) //don't react on anything in first parse.
                            continue;
                        Console.ForegroundColor = ConsoleColor.DarkGray;
                        Console.WriteLine($"log: {line}");
                        Console.ForegroundColor = ConsoleColor.White;

                        var cmd = line.Substring(7);  //cuts off the ingame time
                        var sections = cmd.Split(' ');

                        if (sections.Length > 0 && sections[0].Contains(':')) //if a line containes ':', othervise it's not a event i.e.: 0:00 Session data initialised for client on slot 0 at 21354
                        {
                            var prefixLen = sections[0].Length; //lengts of event word
                            var payload = cmd.Substring(prefixLen); //everything after ':'
                            var eventWord = sections[0].Substring(0, prefixLen - 1); // removing the ':'
                            
                            ParseLine(eventWord, payload); //i.e (say) ( 0 BST|Kipash: !moon on)
                        }
                    }
                }

                firstIteration = false;
            }
        }

        static void ParseLine(string prefix, string payload) 
        {
            LogEvent currentEvent = LogEvent.none;
            if(Enum.TryParse(prefix, out currentEvent) && logEvents.ContainsKey(currentEvent))
            {
                logEvents[currentEvent].Invoke(payload);
            }

        }

        // ~ Oldcode ~ 
        /*
            async Task ProcessLog() 
            {
                bool block = false;

                while (true) 
                {
                    await Task.Delay(500);
                    Console.WriteLine("process log");

                    
                    string logPath = @"C:\Users\Kip\AppData\Roaming\Quake3\q3ut4\game.log";

                    FileStream fs = new FileStream(logPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                    StreamReader sr = new StreamReader(fs);

                    string log = sr.ReadToEnd();

                    if (log.Contains("!moon on") && !log.Contains("!moon off")) 
                    {
                        rconCommands.Enqueue(() => { ReadResponse(SentRconCommand(client, GenerateRconCommand($"{password} g_gravity 100"))); });
                    }
                    else if (log.Contains("!moon on") && log.Contains("!moon off") && !block) 
                    {
                        block = true;
                        rconCommands.Enqueue(() => { ReadResponse(SentRconCommand(client, GenerateRconCommand($"{password} g_gravity 800"))); });
                    }
                }
            }
        */
    }
}
